
var customerDAO = require('../dao/customerdao');

module.exports.save = function(request, response) {

  //var entity = request.body.customer;
  console.log(request.body);
  var entity = request.body;
  customerDAO.save(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetAll = function(request, response) {

  customerDAO.GetAll(function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}


module.exports.autenticarCustomer = function(request, response) {

  var email = request.body.email;
  var password = request.body.password;
  customerDAO.autenticarCustomer(email, password, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           errorCode: error.statusCode,
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
       response.status(200).json(entity);
        // response.status(200).json({
        //    //customer: entity
        //    entity;
        // });
     }

  });
}

module.exports.PostAtualizarTerm = function(request, response) {

  //var entity = request.body.customer;
  console.log(request.body);
  var entity = request.body;
  customerDAO.PostAtualizarTerm(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetAddress = function(request, response ) {

  customerDAO.GetAddress(request, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }
  });
}

module.exports.GetContacts = function(request, response ) {

  customerDAO.GetContacts(request, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }
  });
}
