
var productDAO = require('../dao/productdao');

module.exports.save = function(request, response) {

  //var entity = request.body.customer;
  console.log(request.body);
  var entity = request.body;
  productDAO.save(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetAll = function(request, response) {

  productDAO.GetAll(request, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetById = function(request, response) {
  productDAO.GetById(request.params.id, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetByDescription = function(request, response) {
  productDAO.GetByDescription(request.params.name, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }
  });
}
