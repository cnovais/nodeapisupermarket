"use strict";

module.exports  = function(sequelize, DataTypes){
    var customer =  sequelize.define('customer', {
        name: { type: DataTypes.STRING(150), unique: false },
        password: { type: DataTypes.STRING(200), unique: false },
        confirmpassword: { type: DataTypes.STRING(200), unique: false },
        dateborn: { type: DataTypes.DATE , unique: false },
        term: { type: DataTypes.INTEGER, unique: false, defaultValue: 0 },
        email: { type: DataTypes.STRING(100), unique: true, validate:{
          isEmail: { msg: 'Email invalido' }
          }
        },
        facebook: { type: DataTypes.STRING, unique: false },
        google: { type: DataTypes.STRING, unique: false },
        twitter: { type: DataTypes.STRING, unique: false }
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name
      });

    return customer;
}
