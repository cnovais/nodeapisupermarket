"use strict";

module.exports  = function(sequelize, DataTypes){
    var product =  sequelize.define('product', {
        name: { type: DataTypes.STRING(150), unique: false },
        description: { type: DataTypes.STRING(150), unique: false },
        barcode: { type: DataTypes.STRING(200), unique: true },
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name
        classMethods: {
            associate: function(models) {
                product.belongsTo(models.custom_image, {onDelete: "CASCADE"});
                product.belongsTo(models.product_category, {onDelete: "CASCADE"});
            }
        }
      });

    return product;
}
