var models = require('../models');
var sequelize = models.sequelize;
var orderModel = models.order;
var orderitemModel = models.order_item;

module.exports.save = function(entityorder, callback ) {

  orderModel.create({
          quantity: entityorder.quantity,
          totalprice: entityorder.totalprice,
          cargoprice: entityorder.cargoprice,
          orderitemModels: entityorder.items,
          companyId: entityorder.companyId,
          customerId: entityorder.customerId
      }).then( function(order) {
        entityorder.orderitens.forEach(function(orderitem){
            orderitemModel.create({
              quantity: orderitem.quantity,
              price: orderitem.price,
              orderId: orderitem.orderId,
              productId: orderitem.product.id,
            }).then(function(orderitensInserido){
              // efetua o vinculo
              order.addOrderitens(orderitensInserido).save();
            })
        });

        callback(order, null);
      }).catch( function(error) {
        callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.update = function(entityorder, callback ) {

  orderModel.findOne({
    where: {id: entityorder.id},
    include: [ {model: models.order_item, as: 'orderitens' }]
  }).then( function(order) {

            //atualiza itens pedidos
            order.orderitens.forEach(function(order_item){
              //promises.push(db.User.update({name:user.name},{where : {username:user.username}});
              order_item.update({ status: 1
                }).then(function(result){
                    console.log('atualizado itens do pedido')
                })
                .catch(function(error){
                  callback( null, { statusCode: 600, mensagem: 'FALHA: atualizar itens pedidos => ' + error });
                });
            });

            order.update({
                  quantity: entityorder.quantity,
                  totalprice: entityorder.totalprice,
                  cargoprice: entityorder.cargoprice
              }).then( function(order) {
                      entityorder.orderitens.forEach(function(orderitem){
                          
                          orderitemModel.findById(orderitem.id)
                          .then(function(itemResult){

                              if (itemResult != null) {
                                  itemResult.update({
                                    quantity: orderitem.quantity,
                                    price: orderitem.price,
                                    status: 0
                                  }).then(function(orderitensInserido){
                                    console.log(orderitensInserido)
                                  }).catch( function(error) {
                                    callback( null, { statusCode: 600, mensagem: 'FALHA: atualizacao item pedido => ' + error });
                                  });
                              }else{
                                  orderitemModel.create({
                                    quantity: orderitem.quantity,
                                    price: orderitem.price,
                                    orderId: order.id,
                                    productId: orderitem.product.id,
                                  }).then(function(orderitensInserido){
                                    // efetua o vinculo
                                    order.addOrderiten(orderitensInserido);
                                    //order.save();
                                  });//.complete(function(orderitensInserido){
                                  //    // OrderModel
                                  //     orderModel.findOne({
                                  //       where: {id: entityorder.id},
                                  //       include: [ {model: models.order_item, as: 'orderitens' }]
                                  //     }).then(function(result){
                                  //         callback(result, null);
                                  //     }).catch(function(error){
                                  //         callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error });  
                                  //     }) 
                                  //     // Fim OrderModel
                                  // });
                              }

                          }).catch( function(error) {
                            callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error });
                          });

                      //callback(order, null);
                    });
             }).catch( function(error) {
                callback( null, { statusCode: 500, mensagem: 'FALHA: ao atualizar pedido => ' + error });
             });

    
    callback(order, null);
  }).catch( function(error) {
    callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error });
  });

  // orderitemModel.update(
  //         {status: 1 }, //,}
  //         {where: {
  //           include: [
  //           { model: models.order_orderitem, where: { orderId: 1 }}
  //         ] 
  //         }
  //         // include: [
  //         //   { model: models.order_orderitem, where: { orderId: 1 }}
  //         // ]
  //     }).then( function(orderitem) {
  //       callback(orderitem, null);
  //     }).catch( function(error) {
  //       callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error });
  //     });

  // orderModel.update({
  //         quantity: entityorder.quantity,
  //         totalprice: entityorder.totalprice,
  //         cargoprice: entityorder.cargoprice,
  //         orderitemModels: entityorder.items,
  //     }).then( function(order) {
  //       // entityorder.orderitens.forEach(function(orderitem){
            
  //       //     orderitemModel.findById(123).then(function(project) {
  //       //       // project will be an instance of Project and stores the content of the table entry
  //       //       // with id 123. if such an entry is not defined you will get null
    
  //       //     })
  //       //     orderitemModel.create({
  //       //       quantity: orderitem.quantity,
  //       //       price: orderitem.price,
  //       //       orderId: orderitem.orderId,
  //       //       productId: orderitem.product.id,
  //       //     }).then(function(orderitensInserido){
  //       //       // efetua o vinculo
  //       //       order.addOrderitens(orderitensInserido).save();
  //       //     })
  //       // });

  //       callback(order, null);
  //     }).catch( function(error) {
  //       callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
  //     });
  // });
}

module.exports.GetAll = function(request, callback) {
  //request.params.where
  //where: {status: {in: [0,1,2]}},
  // where: {status: {in: [request.params.where]}},
  var arrayNumber = [];
  var arr = request.query.where.split(",").map(function (val) {
    return parseInt(val);
  });
  console.log("request.params.where cleiton" + arr);
  console.log("request.params.where cleiton" + request.query.where);
  orderModel.findAndCountAll({
     offset: request.query.offset, limit: request.query.limit,
     //attributes:  [ 'id', [sequelize.fn('COUNT'), 'no_hats'] ],
     where: {status: {in: [
       arr
     ]}},
     include: [ {model: models.order_item, as: 'orderitens',
                where: { status: 0 },
                include: {
                            model: models.product
                          }
                 },
                {model: models.customer, as: 'customer' }
      ]
  }).then(function(product) {
        callback(product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetByCustomerId = function(_id, callback) {
  orderModel.findAll({

     where: ["customerId = ? AND status = 0", _id ],
     include: [{model: models.order_item,
       as: 'orderitens',
       include: {
                 model: models.product
               }
        }]
  }).then(function(product) {
        callback(product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.AlterarStatusPedido = function(entity, callback) {
    orderModel.update(
        {status: entity.id_status_pedido },
        {where: { id: entity.order_id } }
      ).then(function (order) {
      callback(order, null);
    }).catch(function (error) {
      callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [AlterarStatusPedido]  => ' + error });
    });
}


module.exports.ExcluirItemProduto = function(entity, callback ) {

  console.log("order_id :     " + entity.order_id);


  orderModel.findAll({
     where: { id: entity.order_id },
     include: [ {model: models.order_item, as: 'orderitens' } ]
  }).then(function(order) {
      console.log("orderitem_id : " + entity.orderitem_id);

      orderitemModel.findById(entity.orderitem_id).then(function(orderitem) {
            orderitem.update({status: 1 }).then(function (order) {
              callback(order, null);
            }).catch(function (error) {
              callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [CustomerModel] Não encontrou o => ' + error });
            })
          }).catch(function(error) {
            callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [CustomerModel] Não encontrou o => ' + error });
          });

      }).catch( function(error) {
        callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error });
      });

}
  //});

  // orderitemModel.update(
  //     { status: 1 },
  //     { where: { _id: id_item_produto } }
  // ).then(function(order_item) {
  //       callback(order_item, null);
  //   }).catch( function(error) {
  //   callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error });
  // });
//}
