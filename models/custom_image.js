"use strict";

module.exports  = function(sequelize, DataTypes){
    var custom_image =  sequelize.define('custom_image', {
        width: { type: DataTypes.INTEGER, unique: false },
        height: { type: DataTypes.INTEGER, unique: false },
        path: { type: DataTypes.STRING(200), unique: false },
        thumbwidth: { type: DataTypes.INTEGER, unique: false },
        thumbheight: { type: DataTypes.INTEGER, unique: false },
        thumbpath: { type: DataTypes.STRING(200), unique: false }    
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name
      });

    return custom_image;
}
