
var companyDAO = require('../dao/companydao');

module.exports.save = function(request, response) {

  //var entity = request.body.customer;
  console.log(request.body);
  var entity = request.body;
  companyDAO.save(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetAll = function(request, response) {

  companyDAO.GetAll(request, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetById = function(request, response) {

  companyDAO.GetById(request.params.id, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}
