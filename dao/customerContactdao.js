var models = require('../models');
var sequelize = models.sequelize;
var CustomerContactModel = models.customer_contact;

//var sequelize = models.sequelize;

module.exports.save = function(entity, callback ) {
  CustomerContactModel.create({
    ddd: entity.ddd,
    number: entity.number,
    type: entity.type,
    customerId: entity.customerId
      }).then( function(CustomerContactModel) {
        callback(CustomerContactModel, null);
      }).catch( function(error) {
        callback( null, { statusCode: 700, mensagem: 'FALHA: 0500: [CustomerContact] Problema com banco de dados, erro => ' + error
      });
  });

}

module.exports.GetAll = function(callback ) {
  console.log("Criando customer DAO");
  CustomerContactModel.findAll({ limit: 10 }).then( function(CustomerContact) {
        callback(CustomerContact, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });

}
