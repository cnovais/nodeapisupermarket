var models = require('../models');
var sequelize = models.sequelize;
var product_categoryModel = models.product_category;

//var sequelize = models.sequelize;

module.exports.save = function(product_category, callback ) {

  product_categoryModel.create({
    name: product_category.name,
    description: product_category.description
  }).then( function(product_category) {
        callback(product_category, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });

}

module.exports.GetAll = function(callback ) {
  console.log("Criando customer DAO");
  product_categoryModel.findAll({
      order: [
         ['id']
      ]
      }).then( function(product_category) {
        callback(product_category, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetById = function(id, callback) {
  product_categoryModel.findAll({
        where: {
          id:id
        }
      }).then( function(product_category) {
        callback(product_category, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}
