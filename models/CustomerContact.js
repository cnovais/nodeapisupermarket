"use strict";

module.exports  = function(sequelize, DataTypes){
    var customer_contact =  sequelize.define('customer_contact', {
        ddd: { type: DataTypes.INTEGER, unique: false, allowNull: false },
        number: { type: DataTypes.STRING(3), unique: false, allowNull: false },
        type: { type: DataTypes.STRING(200), unique: false }
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name

        classMethods: {
            associate: function(models) {
                customer_contact.belongsTo(models.customer, {onDelete: "CASCADE"});
            }
        }
      });

    return customer_contact;
}
