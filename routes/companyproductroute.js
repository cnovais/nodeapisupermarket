// ============= Loading Modules =============
var express = require('express');
var bodyParser = require('body-parser');

var router = express();
var jsonParser = bodyParser.json();

var jsonParser = bodyParser.json()
var parseUrlencoded = bodyParser.urlencoded({extended: false});

//Controller para chamar a logica
var company_producController = require('../controllers/company_productController');


router.route('/')
    .get( function( request, response ) {
       console.log( "Route [GET][/]" );
       company_producController.GetAll(request, response);
    })
    .post(jsonParser, function(request, response) {
        console.log( "Route [POST][/customer]" );
        company_producController.save(request, response);
    }).put(jsonParser, function(request, response) {
        console.log( "Route [POST][/customer]" );
        company_producController.update(request, response);
    });

router.get('/:id', function(request, response){
  company_producController.GetById(request, response);
});

router.get('/company/:id', function(request, response){
  company_producController.GetProductByIdCompany(request, response);
});

router.get('/product/nome/:idcompany/:name', function(request, response){
  company_producController.GetProductByName(request, response);
});

router.get('/product/:idCompany/:idProduto', function(request, response){
  company_producController.GetProductById(request, response);
});

router.get('/product/barcode/:idCompany/:idBarcode', function(request, response){
  company_producController.GetProductByBarcode(request, response);
});

module.exports = router;
