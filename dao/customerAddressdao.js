var models = require('../models');
var sequelize = models.sequelize;
var CustomerAddressModel = models.customer_address;

//var sequelize = models.sequelize;

module.exports.save = function(CustomerAddress, callback ) {

  CustomerAddressModel.create({
    state: CustomerAddress.state,
    city: CustomerAddress.city,
    neighborhood: CustomerAddress.neighborhood,
    street: CustomerAddress.street,
    number: CustomerAddress.number,
    adjunct: CustomerAddress.adjunct,
    zipCode: CustomerAddress.zipCode,
    latitude: CustomerAddress.latitude,
    longitude:CustomerAddress.longitude,
    type: CustomerAddress.type,
    customerId: CustomerAddress.customerId
      }).then( function(CustomerAddress) {
        callback(CustomerAddress, null);
      }).catch( function(error) {
        callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetAll = function(callback ) {
  console.log("Criando customer DAO");
  CustomerAddressModel.findAll({ limit: 10 }).then( function(CustomerAddress) {
        callback(CustomerAddress, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });

}
