"use strict";

module.exports  = function(sequelize, DataTypes){
    var company_product =  sequelize.define('company_product', {
      previousprice: { type: DataTypes.DECIMAL, unique: false },
      price: { type: DataTypes.DECIMAL, unique: false },
      inventoryquantity: { type: DataTypes.INTEGER, unique: false },
      isnewproduct: { type: DataTypes.INTEGER, unique: false },
      soldquantity: { type: DataTypes.INTEGER, unique: false },
      summary: { type: DataTypes.STRING(200), unique: false }
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name

        classMethods: {
            associate: function(models) {
                company_product.belongsTo(models.company, {onDelete: "CASCADE"});
                company_product.belongsTo(models.product, {onDelete: "CASCADE"});
                company_product.belongsTo(models.custom_image, {onDelete: "CASCADE"});
            }
        }

      });

    return company_product;
}
