var models = require('../models');
var sequelize = models.sequelize;
var CustomerModel = models.customer;
var CustomerAddressModel = models.customer_address;
var CustomerContacsModel = models.customer_contact;

//var sequelize = models.sequelize;

module.exports.save = function(customer, callback ) {
  console.log("Criando customer DAO");
  console.log(customer);
  console.log(customer);

  CustomerModel.create({
    name: customer.name,
    email: customer.email,
    password: customer.password,
    confirmpassword: customer.confirmpassword,
    dateborn: customer.dateborn,
    facebook: customer.facebook,
    google: customer.google,
    twitter: customer.twitter,
    term: customer.term
      }).then( function(Customer) {
        // console.log(Customer.get({
        //   plain: true
        // }))
        callback(Customer.get(), null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });

}

module.exports.GetAll = function(callback ) {
  console.log("Criando customer DAO");
  CustomerModel.findAll({
      attributes: ['id', 'name', 'email', 'dateborn','term', 'facebook', 'google','twitter'],
      order: [
         ['id']
      ]
      }).then( function(Customer) {
        callback(Customer, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error});
      });

}

module.exports.autenticarCustomer = function(email, password, callback ) {

   CustomerModel.findOne({
     where: {
       email: email,
       password: password

    }
   })
   .then( function( customer ) {
      console.log("customer: " + customer);
      if ( customer ) {
         callback( customer, null );
      }
      else {
         callback( null, {
            statusCode: 100,
            codigo: 'ERRO100',
            mensagem: "Nao encontrado nenhum resultado"
         });
      }
   }).catch( function(error) {

      callback( null, {
         statusCode: 500,
         codigo: 'GRL01',
         mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });

   }) ;

}


module.exports.PostAtualizarTerm = function(customer, callback ) {
  console.log("Criando customer DAO");
  CustomerModel.update({
        term: customer.term
      },
      {
        where: ["id = ?", customer.id]
      }).then( function(Customer) {
        callback(Customer, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });

}

module.exports.GetAddress = function(request, callback ) {
  CustomerAddressModel.findAll({
      where: ["customerId = ?", request.params.id],
      attributes: ['id', 'state', 'city', 'neighborhood', 'street','number', 'adjunct', 'zipCode','latitude', 'longitude','type'],
      order: [
         ['id']
      ]
    }).then( function(CustomerAddress) {
        callback(CustomerAddress, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetContacts = function(request, callback ) {
  CustomerContacsModel.findAll({
      where: ["customerId = ?", request.params.id],
      attributes: ['id', 'ddd', 'number', 'type'],
      order: [
         ['id']
      ]
    }).then( function(CustomerAddress) {
        callback(CustomerAddress, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}
