"use strict";

module.exports  = function(sequelize, DataTypes){
    var order_item =  sequelize.define('order_item', {
        quantity: { type: DataTypes.INTEGER, unique: false },
        price: { type: DataTypes.DECIMAL, unique: false },
        status: { type: DataTypes.INTEGER, unique: false, defaultValue: 0 }
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name

        classMethods: {
            associate: function(models) {
                order_item.belongsTo(models.product, {onDelete: "CASCADE"});
                order_item.belongsToMany(models.order, {onDelete: "CASCADE", as: 'orderitens', through: 'order_orderitem'});
            }
        }
      });

    return order_item;
}

//status
// 0 = ativo
// 1 = deletado
// 2 = cancelado
