"use strict";

module.exports  = function(sequelize, DataTypes){
    var product_category =  sequelize.define('product_category', {
        name: { type: DataTypes.STRING(150), unique: false },
        description: { type: DataTypes.STRING(150), unique: false }
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name
        classMethods: {
            associate: function(models) {
                product_category.belongsTo(models.custom_image, {onDelete: "CASCADE"});
            }
        }
      });

    return product_category;
}
