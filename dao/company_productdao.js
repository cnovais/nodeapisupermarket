var models = require('../models');
var sequelize = models.sequelize;
var company_productModel = models.company_product;

//var sequelize = models.sequelize;

module.exports.save = function(company_product, callback ) {
  company_productModel.create({
    previousprice: company_product.previousprice,
    price: company_product.price,
    inventoryquantity: company_product.inventoryquantity,
    isnewproduct: company_product.isnewproduct,
    soldquantity: company_product.soldquantity,
    summary: company_product.summary,
    companyId: company_product.companyId,
    productId: company_product.productId,
    custom_imageId: company_product.custom_imageId
  }).then( function(company_product) {
        callback(company_product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.update = function(company_product, callback ) {
  company_productModel.update({
    previousprice: company_product.previousprice,
    price: company_product.price,
    inventoryquantity: company_product.inventoryquantity,
    soldquantity: company_product.soldquantity,
    summary: company_product.summary,
    custom_imageId: company_product.custom_imageId },
    { where: { id: company_product.id } }
  ).then( function(company_product) {
        callback(company_product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetAll = function(request, callback ) {
  console.log("Criando customer DAO");
  company_productModel.findAndCountAll({ offset: request.query.offset, limit: request.query.limit,
    include: [{model: models.product}, {model: models.custom_image}],
  }).then( function(company_product) {
        callback(company_product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetById = function(_id, callback) {
  company_productModel.findById(_id).then( function(company_product) {
        callback(company_product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetProductByIdCompany = function(request, callback) {
  company_productModel.findAll({
    attributes: ['id','previousprice', 'price', 'inventoryquantity', 'isnewproduct', 'soldquantity', 'summary'],
     where: ["companyId = ?", request.params.id],
     include: [{model: models.product}],
     offset: request.query.offset,
     limit: request.query.limit
  }).then( function(company_product) {
        callback(company_product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetProductByName = function(_id, nameproduct, callback) {
  company_productModel.findAndCountAll({
     attributes: ['id','previousprice', 'inventoryquantity', 'isnewproduct', 'soldquantity', 'summary', 'price'],
     where: ["companyId = ?", _id],
     include:[{ model: models.product, where: { name: { $like: "%"+nameproduct+"%" }  } }]
  }).then( function(company_product) {
        console.log("bazingateste" + company_product);
        callback(company_product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetProductByBarcode = function(_idCompany, _idBarcode, callback) {
  company_productModel.findAndCountAll({
     attributes: ['id','previousprice', 'inventoryquantity', 'isnewproduct', 'soldquantity', 'summary', 'price'],
     where: ["companyId = ?", _idCompany],
     include:[{ model: models.product, where: { barcode: { $eq: _idBarcode }  } }]
  }).then( function(company_product) {
        console.log("bazingateste" + company_product);
        callback(company_product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetProductById = function(_idCompany, _idProduto, callback) {
  company_productModel.findAndCountAll({
     attributes: ['id','previousprice', 'inventoryquantity', 'isnewproduct', 'soldquantity', 'summary', 'price'],
     where: ["companyId = ?", _idCompany],
     include:[{ model: models.product, where: { id: { $eq: _idProduto }  } }]
  }).then( function(company_product) {
        console.log("bazingateste passou aqui" + company_product);
        callback(company_product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}
