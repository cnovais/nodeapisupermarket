// ============= Loading Modules =============
var express = require('express');
var bodyParser = require('body-parser');

var router = express();
var jsonParser = bodyParser.json();

var jsonParser = bodyParser.json()
var parseUrlencoded = bodyParser.urlencoded({extended: false});

//Controller para chamar a logica
var customerController = require('../controllers/customerController');


router.route('/')
    .get( function( request, response ) {
       console.log( "Route [GET][/]" );
       customerController.GetAll(request, response);
    })
    .post(jsonParser, function(request, response) {
        console.log( "Route [POST][/customer]" );
        customerController.save(request, response);
});

router.get('/:id', function(req, res){
  res.send('teste' + id);
});

router.route('/autenticar')
.post(jsonParser, function(request, response) {
    console.log( "Route [POST][/customer/autenticar]" );
    //console.log(request.body);
    //response.send('teste' + request.body);
    customerController.autenticarCustomer(request, response);
});

router.route('/term')
.post(jsonParser, function(request, response) {
    customerController.PostAtualizarTerm(request, response);
});

router.get('/address/:id', function(req, res){
  console.log("passou aqui")
  customerController.GetAddress(req, res);
});

router.get('/contacts/:id', function(req, res){
  customerController.GetContacts(req, res);
});

module.exports = router;
