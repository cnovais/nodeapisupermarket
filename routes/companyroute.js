// ============= Loading Modules =============
var express = require('express');
var bodyParser = require('body-parser');

var router = express();
var jsonParser = bodyParser.json();

var jsonParser = bodyParser.json()
var parseUrlencoded = bodyParser.urlencoded({extended: false});

//Controller para chamar a logica
var companyController = require('../controllers/companyController');


router.route('/')
    .get( function( request, response ) {
       console.log( "Route [GET][/]" );
       companyController.GetAll(request, response);
    })
    .post(jsonParser, function(request, response) {
        console.log( "Route [POST][/customer]" );
        companyController.save(request, response);
});

router.get('/:id', function(request, response){
  companyController.GetById(request, response);
});

router.post('/term', function(request, response){
  companyController.GetById(request, response);
});



module.exports = router;
