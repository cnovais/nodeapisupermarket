
var orderDAO = require('../dao/orderdao');

module.exports.save = function(request, response) {

  //var entity = request.body.customer;
  var entity = request.body;
  orderDAO.save(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.update = function(request, response) {

  //var entity = request.body.customer;
  var entity = request.body;
  orderDAO.update(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetByCustomerId = function(request, response) {
  orderDAO.GetByCustomerId(request.params.id, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}


module.exports.GetAll = function(request, response) {

  orderDAO.GetAll(request, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}


module.exports.ExcluirItemProduto = function(request, response) {
var entity = request.body;
  orderDAO.ExcluirItemProduto(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}


module.exports.AlterarStatusPedido = function(request, response) {
  var entity = request.body;
  orderDAO.AlterarStatusPedido(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}
