var models = require('../models');
var sequelize = models.sequelize;
var productModel = models.product;

module.exports.save = function(product, callback ) {

  productModel.create({
    name: product.name,
    description: product.description,
    barcode: product.barcode,
    custom_imageId: product.custom_imageId,
    product_categoryId: product.product_categoryId
      }).then( function(CustomerAddress) {
        callback(CustomerAddress, null);
      }).catch( function(error) {
        callback( null, { statusCode: 600, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}


module.exports.GetAll = function(request, callback ) {
  console.log("Criando customer DAO");
  productModel.findAll({
      offset: request.query.offset, limit: request.query.limit
      }).then( function(product_category) {
        callback(product_category, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetById = function(_id, callback) {
  productModel.findById(_id).then(function(product) {
        callback(product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetByDescription = function(nameProduct, callback) {
  productModel.findAll({
     where: {
        name: {
          $like: "%"+nameProduct+"%",
        }
      }
    }).then(function(product) {
        callback(product, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}
