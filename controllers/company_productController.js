
var company_productdaoDAO = require('../dao/company_productdao');

module.exports.save = function(request, response) {

  //var entity = request.body.customer;
  var entity = request.body;
  company_productdaoDAO.save(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.update = function(request, response) {

  //var entity = request.body.customer;
  var entity = request.body;
  company_productdaoDAO.update(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetAll = function(request, response) {

  company_productdaoDAO.GetAll(request, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetById = function(request, response) {

  company_productdaoDAO.GetById(request.params.id, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetProductByIdCompany = function(request, response) {
  console.log(request.params.id)
  company_productdaoDAO.GetProductByIdCompany(request, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetProductByName = function(request, response) {

  company_productdaoDAO.GetProductByName(request.params.idcompany, request.params.name, function(entity, error) {
    console.log("bazinga" + entity + error);
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {

        response.status(200).json(entity);
     }

  });
}

module.exports.GetProductByBarcode = function(request, response) {

  company_productdaoDAO.GetProductByBarcode(request.params.idCompany, request.params.idBarcode, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}

module.exports.GetProductById = function(request, response) {

  company_productdaoDAO.GetProductById(request.params.idCompany, request.params.idProduto, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}