// ============= Loading Modules =============
var express = require('express');
var bodyParser = require('body-parser');

var router = express();
var jsonParser = bodyParser.json();

var jsonParser = bodyParser.json()
var parseUrlencoded = bodyParser.urlencoded({extended: false});

//Controller para chamar a logica
var orderController = require('../controllers/orderController');


router.route('/')
    .get( function( request, response ) {
       console.log( "Route [GET][/]" );
       orderController.GetAll(request, response);
    })
    .post(jsonParser, function(request, response) {
        console.log( "Route [POST][/order]" );
        orderController.save(request, response);
}).put(jsonParser, function(request, response) {
        console.log( "Route [PUT][/customer]" );
        orderController.update(request, response);
    });

router.get('/customer/:id', function(request, response){
  orderController.GetByCustomerId(request, response);
});

router.get('/description/:name', function(request, response){
  console.log( request.params.name );
  orderController.GetByDescription(request, response);
});

router.route('/AlterarStatusPedido')
.post(jsonParser, function(request, response) {
    orderController.AlterarStatusPedido(request, response);
});


router.route('/ExcluirItemProduto')
.post(jsonParser, function(request, response) {
    orderController.ExcluirItemProduto(request, response);
});



module.exports = router;
