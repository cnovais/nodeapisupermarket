"use strict";

module.exports  = function(sequelize, DataTypes){
    var order =  sequelize.define('order', {
        quantity: { type: DataTypes.INTEGER, unique: false },
        totalprice: { type: DataTypes.DECIMAL, unique: false },
        cargoprice: { type: DataTypes.DECIMAL, unique: false },
        status: { type: DataTypes.INTEGER, unique: false, defaultValue: 0 },
        orderended: { type: DataTypes.DATE, unique: false }
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name
        classMethods: {
            associate: function(models) {
                order.belongsTo(models.company, {onDelete: "CASCADE"});
                order.belongsTo(models.customer, {onDelete: "CASCADE"});
                order.belongsTo(models.customer_address, {onDelete: "CASCADE"});
                order.belongsToMany(models.order_item, {onDelete: "CASCADE", as: 'orderitens', through: 'order_orderitem'});
            }
        }
      });

    return order;
}
