"use strict";

module.exports  = function(sequelize, DataTypes){
    var company =  sequelize.define('company', {
        officialname: { type: DataTypes.STRING(150), unique: false },
        cnpj: { type: DataTypes.STRING(100), unique: false },
        latitude: { type: DataTypes.STRING(200), unique: false },
        longitude: { type: DataTypes.STRING(200), unique: false },
        email: { type: DataTypes.STRING(100), unique: false, validate:{
          isEmail: { msg: 'Email invalido' }
          }
        }
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name
        classMethods: {
            associate: function(models) {
                company.belongsTo(models.custom_image, {onDelete: "CASCADE"});
            }
        }
      });

    return company;
}
