var models = require('../models');
var sequelize = models.sequelize;
var companyModel = models.company;

//var sequelize = models.sequelize;

module.exports.save = function(company, callback ) {
  companyModel.create({
    officialname: company.officialname,
    cnpj: company.cnpj,
    latitude: company.latitude,
    longitude: company.longitude,
    email: company.email
  }).then( function(company) {
        callback(company, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });

}

module.exports.GetAll = function(callback ) {
  console.log("Criando customer DAO");
  companyModel.findAll({
      order: [
         ['id']
      ]
      }).then( function(company) {
        callback(company, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}

module.exports.GetById = function(_id, callback) {
  companyModel.findById(_id).then( function(company) {
        callback(company, null);
      }).catch( function(error) {
        callback( null, { statusCode: 500, mensagem: 'FALHA: 0500: [CustomerModel] Problema com banco de dados, erro => ' + error
      });
  });
}
