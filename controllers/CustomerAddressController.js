
var customerAddressDAO = require('../dao/customerAddressdao');

module.exports.save = function(request, response) {


  var entity = request.body;
  customerAddressDAO.save(entity, function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json({
           status: true,
           customer: entity
        });
     }

  });
}


module.exports.GetAll = function(request, response) {

  customerAddressDAO.GetAll(function(entity, error) {
     if ( error ) {
        response.status(error.statusCode).json({
           status: false,
           mensagem: error.mensagem
        });
     }
     else {
        response.status(200).json(entity);
     }

  });
}
