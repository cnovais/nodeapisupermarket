// ============= Loading Modules =============
var express = require('express');
var bodyParser = require('body-parser');

var router = express();
var jsonParser = bodyParser.json();

var jsonParser = bodyParser.json()
var parseUrlencoded = bodyParser.urlencoded({extended: false});

//Controller para chamar a logica
var productController = require('../controllers/productController');


router.route('/')
    .get( function( request, response ) {
       console.log( "Route [GET][/]" );
       productController.GetAll(request, response);
    })
    .post(jsonParser, function(request, response) {
        console.log( "Route [POST][/customer]" );
        productController.save(request, response);
});

router.get('/:id', function(request, response){
  productController.GetById(request, response);
});

router.get('/description/:name', function(request, response){
  console.log( request.params.name );
  productController.GetByDescription(request, response);
});



module.exports = router;
