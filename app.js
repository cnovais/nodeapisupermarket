// =================================================================
// Loading Modules
// =================================================================
var express = require('express');
var logger = require('morgan');
var models  = require('./models');

// =================================================================
// Global Variables
// =================================================================
var app = express();
var port = process.env.PORT || 7080;


// =================================================================
// Setting Logger
// =================================================================
app.use(logger('dev'));


// =================================================================
// Setting Models
// =================================================================
app.set('models', models);


// =================================================================
// Routes Modules
// =================================================================
 var setupRoute = require('./routes/setuproute');
// var instituicaoRoute = require('./routes/instituicaoroute');
// var pessoaRoute = require('./routes/pessoaroute');
// var usuarioRoute = require('./routes/usuarioroute');
// var turmaRoute = require('./routes/turmaroute');
// var disciplinaRoute = require('./routes/disciplinaroute');
// var professorRoute = require('./routes/professorroute');
// var responsavelRoute = require('./routes/responsavelroute');
// var alunoRoute = require('./routes/alunoroute');
// var mensagemRoute = require('./routes/mensagemroute');

//novas
var customerRoute = require('./routes/customerroute');
var customercontactRoute = require('./routes/customercontactroute');
var customeraddressRoute = require('./routes/customeraddressroute');
var companyroute = require('./routes/companyroute');
var companyproductroute = require('./routes/companyproductroute');
var productroute = require('./routes/productroute');
var orderroute = require('./routes/orderroute');



// =================================================================
// Settings Express
// =================================================================
app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,x-access-token,x-key');
  // If someone calls with method OPTIONS, let's display the allowed methods on our API
  if (req.method == 'OPTIONS') {
    res.status(200);
    res.write("Allow: GET,PUT,POST,DELETE,OPTIONS");
    res.end();
  } else {
    next();
  }

});


// =================================================================
// Settings Routes to App
// =================================================================
 app.use( '/setup', setupRoute );
 
app.use( '/customer', customerRoute);
app.use( '/customercontact', customercontactRoute);
app.use( '/customeraddress', customeraddressRoute);
app.use( '/company', companyroute);
app.use( '/companyproduct', companyproductroute);
app.use( '/product', productroute);
app.use( '/order', orderroute);



// =================================================================
// Start Server
// =================================================================
app.listen(port, function() {
    console.log( "Servidor Middleware Certain Price listn in port: " + port );
});
