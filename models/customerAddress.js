"use strict";

module.exports  = function(sequelize, DataTypes){
    var customer_address =  sequelize.define('customer_address', {
        state: { type: DataTypes.STRING(150), unique: false, allowNull: false },
        city: { type: DataTypes.STRING(200), unique: false },
        neighborhood: { type: DataTypes.STRING(200), unique: false },
        street: { type: DataTypes.STRING(200), unique: false, allowNull: false },
        number: { type: DataTypes.STRING(200), unique: false, allowNull: false },
        adjunct: { type: DataTypes.STRING(200), unique: false },
        zipCode: { type: DataTypes.STRING(200), unique: false },
        latitude: { type: DataTypes.STRING(200), unique: false },
        longitude: { type: DataTypes.STRING(200), unique: false },
        type: { type: DataTypes.STRING(200), unique: false }
      }, {
        freezeTableName: true, // Model tableName will be the same as the model name

        classMethods: {
            associate: function(models) {
                customer_address.belongsTo(models.customer, {onDelete: "CASCADE"});
            }
        }

      });

    return customer_address;
}

// type
// Residencial
// Entrega