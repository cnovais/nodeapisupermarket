module.exports = {

	'ERR01': 'Registro não localizado',
	'ERR02': 'Parâmetro não informado',
	'ERR03': 'Registro existente',
  'GRL01': 'Falha para executar o comando no banco de dados'

};
